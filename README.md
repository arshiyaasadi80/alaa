# alaa App (alaa)

Test project for alaa

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in development mode

```bash
yarn dev
# or
npm dev
```

### Build the app for production

```bash
yarn build
# or
npm build
```
