const routes = [
  {
    path: "/auth",
    children: [{ path: "", component: () => import("pages/auth/AuthIndexPage.vue") }],
  },

  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [{ path: "", component: () => import("pages/IndexPage.vue") }],
  },
  {
    path: "/profile",
    component: () => import("layouts/MainLayout.vue"),
    children: [{ path: "", component: () => import("pages/profilePage.vue") }],
  },
];

export default routes;
