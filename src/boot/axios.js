import { boot } from "quasar/wrappers";
import axios from "axios";

const api = axios.create({ baseURL: "https://alaatv.com/api/v2/" });

export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$axios = axios
  app.config.globalProperties.$api = api
});

export { api };
